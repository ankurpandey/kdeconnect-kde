# Translation of kdeconnect-kcm.po to Ukrainian
# Copyright (C) 2013-2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2013, 2014, 2015, 2016, 2017, 2018, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-31 00:46+0000\n"
"PO-Revision-Date: 2023-06-03 08:50+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Юрій Чорноіван"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "yurchor@ukr.net"

#: kcm.cpp:183
#, kde-format
msgid "Key: %1"
msgstr "Ключ: %1"

#: kcm.cpp:206
#, kde-format
msgid "Available plugins"
msgstr "Доступні додатки"

#: kcm.cpp:253
#, kde-format
msgid "Error trying to pair: %1"
msgstr "Помилка під час спроби пов’язати: %1"

#: kcm.cpp:269
#, kde-format
msgid "(paired)"
msgstr "(пов’язано)"

#: kcm.cpp:272
#, kde-format
msgid "(not paired)"
msgstr "(не пов’язано)"

#: kcm.cpp:275
#, kde-format
msgid "(incoming pair request)"
msgstr "(вхідний запит щодо прив’язування)"

#: kcm.cpp:278
#, kde-format
msgid "(pairing requested)"
msgstr "(отримано запит щодо прив’язування)"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:59
#, kde-format
msgid "KDE Connect"
msgstr "З’єднання KDE"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:82
#, kde-format
msgid "Edit"
msgstr "Змінити"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:104
#, kde-format
msgid "Save"
msgstr "Зберегти"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:120
#, kde-format
msgid "Refresh"
msgstr "Оновити"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:193
#, kde-format
msgid "Device"
msgstr "Пристрій"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:209
#, kde-format
msgid "(status)"
msgstr "(стан)"

#. i18n: ectx: property (text), widget (KSqueezedTextLabel, verificationKey)
#: kcm.ui:232
#, kde-format
msgid "🔑 abababab"
msgstr "🔑 abababab"

#. i18n: ectx: property (text), widget (QPushButton, cancel_button)
#: kcm.ui:263
#, kde-format
msgid "Cancel"
msgstr "Скасувати"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:289
#, kde-format
msgid "Accept"
msgstr "Прийняти"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:296
#, kde-format
msgid "Reject"
msgstr "Відхилити"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:309
#, kde-format
msgid "Request pair"
msgstr "Запит щодо пов’язування"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:322
#, kde-format
msgid "Unpair"
msgstr "Скасувати пов’язування"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:335
#, kde-format
msgid "Send ping"
msgstr "Надіслати сигнал підтримання зв’язку"

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:373
#, kde-format
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
"available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">from F-Droid</span></a>) and it should appear in the list.<br><br>If you "
"are having problems, visit the <a href=\"https://userbase.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"Community wiki</span></a> for help.</p></body></html>"
msgstr ""
"<html><head/><body><p>Не вибрано пристрою.<br><br>Якщо у вас є телефон під "
"керуванням Android, встановіть <a href=\"https://play.google.com/store/apps/"
"details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">програму KDE Connect для Android</span></a> "
"(також доступна з <a href=\"https://f-droid.org/repository/browse/?fdid=org."
"kde.kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">F-Droid</span></a>), і вона з’явиться у списку.<br><br>Якщо у вас "
"виникають проблеми, відвідайте <a href=\"https://userbase.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">вікі "
"спільноти KDE Connect</span></a>, щоб дізнатися більше.</p></body></html>"

#~ msgid "KDE Connect Settings"
#~ msgstr "Параметри KDE Connect"

#~ msgid "KDE Connect Settings module"
#~ msgstr "Модуль параметрів KDE Connect"

#~ msgid "(C) 2015 Albert Vaca Cintora"
#~ msgstr "© Albert Vaca Cintora, 2015"

#~ msgid "Albert Vaca Cintora"
#~ msgstr "Albert Vaca Cintora"

#~ msgid "(C) 2018 Nicolas Fella"
#~ msgstr "© Nicolas Fella, 2018"

#~ msgid "No device selected."
#~ msgstr "Пристроїв не позначено."

#~ msgid ""
#~ "<html><head/><body><p>If you are having problems, visit the <a href="
#~ "\"https://community.kde.org/KDEConnect\"><span style=\" text-decoration: "
#~ "underline; \">KDE Connect Community wiki</span></a> for help.</p></body></"
#~ "html>"
#~ msgstr ""
#~ "<html><head/><body><p>Якщо у вас виникають проблеми, спробуйте знайти "
#~ "довідкову інформацію на сторінці <a href=\"https://community.kde.org/"
#~ "KDEConnect\"><span style=\" text-decoration: underline; \">KDE Connect у "
#~ "вікі спільноти</span></a>.</p></body></html>"

#~ msgid "(trusted)"
#~ msgstr "(надійний)"

#~ msgid "Unavailable plugins"
#~ msgstr "Недоступні додатки"

#~ msgid "Plugins unsupported by the device"
#~ msgstr "Додатки, підтримки яких на цьому пристрої не передбачено"

#~ msgid "Plugins"
#~ msgstr "Додатки"

#~ msgid "Browse"
#~ msgstr "Переглянути"

#~ msgid "Form"
#~ msgstr "Форма"
