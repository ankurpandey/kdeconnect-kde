# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Kheyyam Gojayev <xxmn77@gmail.com>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-16 00:45+0000\n"
"PO-Revision-Date: 2023-06-12 17:07+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.2\n"

#: main.cpp:32 main.cpp:34
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:36
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(c) 2015, Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Maintainer"
msgstr "Müşayətçi"

#: main.cpp:38
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xəyyam Qocayev"

#: main.cpp:38
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xxmn77@gmail.com"

#: main.cpp:54
#, kde-format
msgid "URL to share"
msgstr "Paylaşmaq üçün URL"

#: qml/DevicePage.qml:23
#, kde-format
msgid "Unpair"
msgstr "Ayırmaq"

#: qml/DevicePage.qml:28
#, kde-format
msgid "Send Ping"
msgstr "Dümsükləmək"

#: qml/DevicePage.qml:36 qml/PluginSettings.qml:15
#, kde-format
msgid "Plugin Settings"
msgstr "Plaqin ayarları"

#: qml/DevicePage.qml:60
#, kde-format
msgid "Multimedia control"
msgstr "Multimedia idarə edilməsi"

#: qml/DevicePage.qml:67
#, kde-format
msgid "Remote input"
msgstr "Məsafədən yazmaq"

#: qml/DevicePage.qml:74 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Uzaqdan Təqdimat"

#: qml/DevicePage.qml:83 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Kilidləmək"

#: qml/DevicePage.qml:83
#, kde-format
msgid "Unlock"
msgstr "Kiliddən çıxarmaq"

#: qml/DevicePage.qml:90
#, kde-format
msgid "Find Device"
msgstr "Cihazı tapmaq"

#: qml/DevicePage.qml:95 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Əmr başlatmaq"

#: qml/DevicePage.qml:104
#, kde-format
msgid "Send Clipboard"
msgstr "Mübadilə yaddaşını göndərin"

#: qml/DevicePage.qml:110
#, kde-format
msgid "Share File"
msgstr "Fayl paylaşmaq"

#: qml/DevicePage.qml:115 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Səs səviyyəsini tənzimləmək"

#: qml/DevicePage.qml:124
#, kde-format
msgid "This device is not paired"
msgstr "Cihaz qoşulmayıb"

#: qml/DevicePage.qml:128
#, fuzzy, kde-format
#| msgid "Pair"
msgctxt "Request pairing with a given device"
msgid "Pair"
msgstr "Qoşmaq"

#: qml/DevicePage.qml:135 qml/DevicePage.qml:144
#, kde-format
msgid "Pair requested"
msgstr "Qoşulma soruşuldu"

#: qml/DevicePage.qml:150
#, kde-format
msgid "Accept"
msgstr "Qəbul etmək"

#: qml/DevicePage.qml:156
#, kde-format
msgid "Reject"
msgstr "Çıxartmaq"

#: qml/DevicePage.qml:165
#, kde-format
msgid "This device is not reachable"
msgstr "Bu cihaz əlçatan deyil"

#: qml/DevicePage.qml:173
#, kde-format
msgid "Please choose a file"
msgstr "Lütfən faylı seçin"

#: qml/FindDevicesPage.qml:23
#, fuzzy, kde-format
#| msgid "Find Device"
msgctxt "Title of the page listing the devices"
msgid "Devices"
msgstr "Cihazı tapmaq"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Cihaz tapılmadı"

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "Yadda saxlanılanlar"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "Mövcuddur"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "Qoşuldu"

#: qml/main.qml:73
#, kde-format
msgid "Find devices..."
msgstr "Cihazları tapmaq..."

#: qml/main.qml:118 qml/main.qml:121
#, kde-format
msgid "Settings"
msgstr "Ayarlar"

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Uzaqdan idarə"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press %1 or the left and right mouse buttons at the same time to unlock"
msgstr ""
"Kiliddən çıxarmaq üçün %1 və ya siçanın sağ və sol düyməsini eyni zamanda "
"basın"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Multimedia idarəsi"

#: qml/mpris.qml:65
#, kde-format
msgid "No players available"
msgstr "Pleyerlər yoxdur"

#: qml/mpris.qml:105
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "Tam ekran rejimi"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "Əmrlərə düzəliş"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Qoşulan cihazda əmrlərə düzəliş edə bilərsiniz"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "Əmrlər təyin edilməyib"

#: qml/Settings.qml:13
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "Ayarlar"

#: qml/Settings.qml:21
#, kde-format
msgid "Device name"
msgstr "Cihazın adı"

#: qml/Settings.qml:36
#, kde-format
msgid "About KDE Connect"
msgstr "KDE Connect haqqında"

#: qml/Settings.qml:47
#, fuzzy, kde-format
#| msgid "About KDE Connect"
msgid "About KDE"
msgstr "KDE Connect haqqında"
